# Maintainer: Fabian Bornschein <fabiscafe-cat-mailbox-dog-org>
# Contributor: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Jan de Groot <jgc@archlinux.org>

pkgbase=nautilus
pkgname=(nautilus43 libnautilus43-extension-2)
pkgver=43.alpha
pkgrel=1
pkgdesc="Default file manager for GNOME"
url="https://wiki.gnome.org/Apps/Files"
arch=(x86_64)
license=(GPL)
depends=(libgexiv2 gnome-desktop-4 gvfs dconf tracker3 tracker3-miners
         gnome-autoar gst-plugins-base-libs libadwaita libportal-gtk4)
makedepends=(gobject-introspection git gtk-doc meson appstream-glib)
checkdepends=(python-gobject)
options=(debug)
_commit=3bb9c98d705571749e25ba933311b6237c19fc07  # tags/43.alpha^0
source=("git+https://gitlab.gnome.org/GNOME/nautilus.git#commit=$_commit"
        "git+https://gitlab.gnome.org/GNOME/libgd.git")
sha256sums=('SKIP'
            'SKIP')

pkgver() {
  cd $pkgbase
  git describe --tags | sed 's/[^-]*-g/r&/;s/-/+/g'
}

prepare() {
  cd $pkgbase

  git submodule init
  git submodule set-url subprojects/libgd "$srcdir/libgd"
  git submodule update
}

build() {
  arch-meson $pkgbase build \
    -D docs=true \
    -D packagekit=false
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs
}

_pick() {
  local p="$1" f d; shift
  for f; do
    d="$srcdir/$p/${f#$pkgdir/}"
    mkdir -p "$(dirname "$d")"
    mv "$f" "$d"
    rmdir -p --ignore-fail-on-non-empty "$(dirname "$f")"
  done
}

package_nautilus43() {
  depends+=(libnautilus43-extension-2)
  optdepends=('nautilus-sendto: Send files via mail extension')
  conflicts=(nautilus "libnautilus-extension<=42.6")
  provides=(nautilus)
  replaces=(nautilus)
  groups=(gnome)

  meson install -C build --destdir "$pkgdir"

  cd "$pkgdir"

  _pick libne usr/include
  _pick libne usr/lib/{girepository-1.0,libnautilus-extension*,pkgconfig}
  _pick libne usr/share/{gir-1.0,gtk-doc}
}

package_libnautilus43-extension-2() {
  pkgdesc="Library for extending the $pkgdesc"
  depends=(gtk4)
  conflicts=("nautilus<=42.6" libnautilus-extension)
  provides=(libnautilus-extension libnautilus-extension.so)
  replaces=(libnautilus-extension)
  mv libne/* "$pkgdir"
}

# vim:set sw=2 et:
